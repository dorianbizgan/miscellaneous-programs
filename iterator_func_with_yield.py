''' Instructions
_________________

- Acts like range runction
- Example use:

x = yrange(5):
next(x) #prints 0
next(x) #prints 1
next(x) #prints 2
...
...
next(x) #error no more to iterate
'''

def yrange(n):

	i = 0
	while i < n:
		yield i #allows for iteration
		i += 1

def main():

	x = yrange(5)
	print(next(x))
	print(next(x))
	print(next(x))
	print(next(x))
	print(next(x))
	
main()

